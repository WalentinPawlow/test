import java.io.*;
import java.util.*;

class SortNumbersFromFile
{
public static void main(String[] args)
{
    int[] arr = null;
    try (BufferedReader in = new BufferedReader(new FileReader("input.txt")))
    {
        arr = in.lines().mapToInt(Integer::parseInt).toArray();
    }
    catch (IOException | NumberFormatException e)
    {
        e.printStackTrace();
    }
    if (arr != null)
    {
        Arrays.sort(arr);
        for(int i = 0; i <  arr.length; i++) {
        System.out.print(arr[i] + "  ");
        }
    }
}
}